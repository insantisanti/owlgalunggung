;----------------------------------------------
; OwlGalunggung Windows NSIS Checksum Header
; [Checksums.nsh]
; 
; Copyright (C) 2017-2018 Hyang Language Foundation
; Elvira Larasati <laras@owlgalunggung.org>
; Anna Nita <annita@owlgalunggung.org>
; Aldi Dinata <dinataldi@owlgalunggung.org>
;----------------------------------------------

!define MD5_gtk+3.6.4_bin.tbz2	"696ae056ecb12f2893eb203457418cfa"
!define MD5_gtk+3.6.4_bin_full.tbz2	"1163eeeb111e17d0d5133d50b0f2c023"
!define MD5_gtk+3.6.4-1_bin_full.tbz2	"93c66f815d07eab060d3e24b5bf1f84a"
!define MD5_gtk2-runtime-2.24.8-2011-12-03-ash.exe	"570ce4b0bf5c0982f106d104ac23290d"
!define MD5_gtk2-runtime-2.16.6-2010-05-12-ash.exe	"714de69347290ce93bbcbae76aa00bf9"
!define MD5_python-2.7.3.msi "c846d7a5ed186707d3675564a9838cc2"

!define MD5_de_20030222-1	"be9ceae1fc6bc7c78490ce7ea5cea7dd"
!define MD5_de-alt_2.1-1	"f4eca08f44b50eab92c913bf6294ee1b"
!define MD5_en_6.0-0	"a11f5fbb5cf1197383404391e0975c63"
!define MD5_en_7.1-0 "a3b13a7238c2a7742a832a02f12f75cd"
!define MD5_id_1.2-0	"0132596602aba9fab15de6768459850f"
!define MD5_it_2.2_20050523-0	"67e01caf81207561ea89e12b96c10f5a"
!define MD5_ru_0.99f7-1	"10cd6c2362a9189dfda6e019175ea244"
