/* OwlGalunggung Code Editor
 * plugins.h - plugin structure
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PLUGINS_H__
#define __PLUGINS_H__

#include "owlgalunggung.h"

#define OWLGALPLUGIN_PRIORITY_FIRST 0
#define OWLGALPLUGIN_PRIORITY_EARLY 63
#define OWLGALPLUGIN_PRIORITY_DEFAULT 127
#define OWLGALPLUGIN_PRIORITY_LATE 191
#define OWLGALPLUGIN_PRIORITY_LAST 255

#define OWLGALPLUGIN_VERSION 9

typedef struct {
	const gchar *name; /* plugin name */
	const gushort owlgalplugin_version;
	const gushort gtkmajorversion;
	const gushort document_size;  /* the sizes of the most important structs are checker here to avoid a plugin */
	const gushort sessionvars_size;    /* compiled with a different struct accessing the wrong member of a struct  */
	const gushort globalsession_size;
	const gushort owlgalwin_size;
	const gushort project_size;
	const gushort main_size;
	const gushort properties_size;
	const gushort priority;
	const gushort default_enabled;
	
	gpointer private; /* to be filled by OwlGalunggung after loading */
	void (*init) (void); /* called once after plugin is loaded */
	void (*init_gui) (Towlgalwin *owlgalwin); /* builds the gui */
	void (*enforce_session) (Towlgalwin *owlgalwin); /* may change the gui to enforce some session setting */
	void (*cleanup) (void);
	void (*cleanup_gui) (Towlgalwin *owlgalwin);
	
	GHashTable *(*register_globses_config)(GHashTable *configlist);
	GHashTable *(*register_session_config)(GHashTable *configlist, Tsessionvars *session);
	void (*session_cleanup) (Tsessionvars *session);
	gpointer extra1; /* for binary compatibility */
	gpointer extra2;
	gpointer extra3;
} TOwlGalunggungPlugin;

#define OWLGALPLUGIN(var) ((TOwlGalunggungPlugin *)var)

#ifdef __cplusplus
extern "C" {
#endif

void owlgalunggung_load_plugins(void);
void owlgalunggung_cleanup_plugins(void);

void owlgalplugins_gui(gpointer data, gpointer user_data);
void owlgalplugins_enforce_session(gpointer data, gpointer user_data);

GHashTable *owlgalplugins_register_globses_config(GHashTable *list);
GHashTable *owlgalplugins_register_session_config(GHashTable *list,Tsessionvars *session);
void owlgalplugins_session_cleanup(Tsessionvars *session);
#ifdef __cplusplus
};
#endif

#endif /* __PLUGINS_H__ */
