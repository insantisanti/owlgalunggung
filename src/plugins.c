/* OwlGalunggung Code Editor
 * plugins.c - handle plugin loading
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* #define DEBUG */

#include "owl_config.h"

#include <gtk/gtk.h>
#include <gmodule.h>
#include <string.h>
#include "owlgalunggung.h"
#include "owlgal_lib.h"
#include "plugins.h"
#include "stringlist.h"
#ifdef MAC_INTEGRATION
#include <gtkosxapplication.h>
#endif

typedef struct {
	gchar *filename;
	GModule *module;
} TPrivatePluginData;

#define PRIVATE(var) ((TPrivatePluginData *)var->private)

static TOwlGalunggungPlugin *load_plugin(const gchar *filename) {
	GModule *module;
	TOwlGalunggungPlugin *owlgalplugin;
	gpointer func;
	TOwlGalunggungPlugin *(*getplugin)();
	
	module = g_module_open(filename,0);
	if (!module) {
		g_warning("failed to load plugin %s with error %s\n",filename, g_module_error());
		return NULL;
	}

	if (!g_module_symbol(module, "getplugin",&func)) {
		DEBUG_MSG("load_plugin: module %s does not have getplugin(): %s\n",filename,g_module_error());
		g_module_close(module);
		return NULL;
	}
	getplugin = func;
	owlgalplugin = getplugin();
	if (!owlgalplugin) {
		DEBUG_MSG("load_plugin: %s getplugin() returned NULL\n",filename);
		g_module_close(module);
		return NULL;
	}
	owlgalplugin->private = g_new0(TPrivatePluginData,1);
	PRIVATE(owlgalplugin)->filename = g_strdup(filename);
	PRIVATE(owlgalplugin)->module = module;
	return owlgalplugin;
}

static TOwlGalunggungPlugin *plugin_from_filename(const gchar *path) {
	TOwlGalunggungPlugin *owlgalplugin;
	GSList *tmpslist;
	
	owlgalplugin = load_plugin(path);
	DEBUG_MSG("plugin_from_filename, owlgalplugin=%p\n",owlgalplugin);
	if (!owlgalplugin)
		return NULL;
	
	if (!(owlgalplugin->owlgalplugin_version == OWLGALPLUGIN_VERSION
					&& owlgalplugin->gtkmajorversion == GTK_MAJOR_VERSION
					&& owlgalplugin->document_size == sizeof(Tdocument)
					&& owlgalplugin->sessionvars_size == sizeof(Tsessionvars)
					&& owlgalplugin->globalsession_size == sizeof(Tglobalsession)
					&& owlgalplugin->owlgalwin_size == sizeof(Towlgalwin)
					&& owlgalplugin->project_size == sizeof(Tproject)
					&& owlgalplugin->main_size == sizeof(Tmain)
					&& owlgalplugin->properties_size == sizeof(Tproperties)
			)) {
		g_warning("plugin %s is not compatible with this version of owlgalunggung",path);
		DEBUG_MSG("plugin_from_filename, %s binary compatibility mismatch\n",path);
		g_module_close(PRIVATE(owlgalplugin)->module);
		return NULL;
	}
	
	for(tmpslist=main_v->plugins;tmpslist;tmpslist=g_slist_next(tmpslist)) {
		if (g_strcmp0(owlgalplugin->name, ((TOwlGalunggungPlugin *)tmpslist->data)->name)==0) {
			g_warning("not loading %s, plugin \"%s\" was already loaded",path, owlgalplugin->name);
			g_module_close(PRIVATE(owlgalplugin)->module);
			return NULL;
		}
	}
	return owlgalplugin;
}

static void
owlgalunggung_scan_dir_load_plugins(GList **oldlist,const gchar *indirname)
{
	GError *error = NULL;
	GPatternSpec* patspec;
	GDir* gdir;
	const gchar *tmp;
	gchar *dirname = ending_slash(indirname);

	DEBUG_MSG("owlgalunggung_scan_dir_load_plugins, loading from %s\n",dirname);
	DEBUG_PATH("plugins are loaded from %s\n",dirname);
	gdir = g_dir_open(dirname ,0,&error);
	if (error) {
		g_warning("Error: failed to open plugin directory %s",dirname);
		g_error_free(error);
		g_free(dirname);
		return;
	}
	patspec = g_pattern_spec_new("*."G_MODULE_SUFFIX);

	tmp = g_dir_read_name(gdir);
	while (tmp) {
		if (g_pattern_match(patspec, strlen(tmp), tmp, NULL)) {
			gchar *path = g_strconcat(dirname, tmp, NULL);
			gchar *compare[] = {path, NULL}, **arr;

			arr = arraylist_value_exists(*oldlist, (const gchar **) compare, 1, FALSE);
			if (arr) {
				GList *link;
				DEBUG_MSG("owlgalunggung_scan_dir_load_plugins, found %s in configfile (len(oldlist)=%d, len(plugin_config=%d)\n",arr[0],g_list_length(*oldlist),g_list_length(main_v->props.plugin_config));
				link = g_list_find(*oldlist,arr);
				*oldlist = g_list_remove_link(*oldlist, link);
				main_v->props.plugin_config = g_list_concat(main_v->props.plugin_config, link);
			}
			if (arr && arr[1][0] == '0') {
				DEBUG_MSG("owlgalunggung_scan_dir_load_plugins, plugin %s is disabled\n",path);
			} else {
				TOwlGalunggungPlugin *plugin;
				DEBUG_MSG("owlgalunggung_scan_dir_load_plugins, try loading plugin %s\n",path);
				plugin = plugin_from_filename(path);
				if (plugin) {
					if (!arr) {
						if (plugin->default_enabled) {
							arr = array_from_arglist(path, "1", plugin->name, NULL);
							plugin->init();
							main_v->plugins = g_slist_prepend(main_v->plugins,plugin);
						} else {
							arr = array_from_arglist(path, "0", plugin->name, NULL);
							g_module_close(PRIVATE(plugin)->module);
						}
						main_v->props.plugin_config = g_list_append(main_v->props.plugin_config, arr);
					} else {
						plugin->init();
						main_v->plugins = g_slist_prepend(main_v->plugins,plugin);
					}
				} else { /* no plugname ==> failed to load */
					DEBUG_MSG("owlgalunggung_scan_dir_load_plugins, returned NULL -> load failed\n");
					if (arr && g_strv_length(arr)>=2) {
						g_free(arr[1]);
						arr[1] = g_strdup("1");
					} else {
						arr = array_from_arglist(path, "1", "error loading plugin", NULL);
						main_v->props.plugin_config = g_list_append(main_v->props.plugin_config, arr);
					}
				}
			}
			g_free(path);
		}
		tmp = g_dir_read_name(gdir);
	}
	g_dir_close(gdir);
	g_pattern_spec_free(patspec);
	g_free(dirname);
}

static gint plugins_compare_priority(gpointer a, gpointer b) {
	TOwlGalunggungPlugin *pa=a,*pb=b;
	return pa->priority-pb->priority;
}

void owlgalunggung_load_plugins(void) {
	GList *oldlist;
	gchar *path;	
	oldlist = main_v->props.plugin_config;
	DEBUG_MSG("owlgalunggung_load_plugins, oldlist %p len=%d\n",oldlist,g_list_length(oldlist));
	main_v->props.plugin_config = NULL;
	
#ifdef MAC_INTEGRATION
	if (gtkosx_application_get_bundle_id ())
	{
		gchar *owlgalunggung_plugin_dir;
		gchar *bundle_resource_dir = gtkosx_application_get_resource_path ();
		owlgalunggung_plugin_dir = g_build_filename (bundle_resource_dir,
						     "lib",
						     PACKAGE,
						     NULL);
		owlgalunggung_scan_dir_load_plugins(&oldlist,owlgalunggung_plugin_dir);
		DEBUG_MSG("plugin_init_for_MacOSX, plugin path=%s\n", owlgalunggung_plugin_dir);
		g_free(owlgalunggung_plugin_dir);
		g_free(bundle_resource_dir);
	}
#else
	
	/* load from the user directory first */
	path = g_strconcat(g_get_home_dir(), "/."PACKAGE"/",NULL);
	owlgalunggung_scan_dir_load_plugins(&oldlist,path);
	g_free(path);

#if defined(NSIS) || defined(OSXAPP)
#ifdef RELPLUGINPATH
	DEBUG_MSG("using RELPLUGINPATH\n");
	path = g_build_path(G_DIR_SEPARATOR_S,RELPLUGINPATH,"lib",PACKAGE,NULL);
	owlgalunggung_scan_dir_load_plugins(&oldlist, path);
	g_free(path);
#else /* RELPLUGINPATH */
	path = g_build_path(G_DIR_SEPARATOR_S,".","lib",PACKAGE,NULL);
	owlgalunggung_scan_dir_load_plugins(&oldlist, path);
	g_free(path);
#endif /* RELPLUGINPATH */
#elif defined WIN32 
		gchar *topdir;

		topdir = g_win32_get_package_installation_directory_of_module(NULL);
		path = g_build_path(G_DIR_SEPARATOR_S, topdir, "lib", PACKAGE, NULL);
		owlgalunggung_scan_dir_load_plugins(&oldlist, path);
		
		g_free(topdir);
		g_free(path);
#else /* WIN32 */
	owlgalunggung_scan_dir_load_plugins(&oldlist,PKGLIBDIR);
#endif /* NSIS || OSXAPP */
#endif /* MAC_INTEGRATION */

	free_arraylist(oldlist);
	main_v->plugins = g_slist_sort(main_v->plugins,(GCompareFunc)plugins_compare_priority);
}

/* can be called by g_list_foreach() */
void owlgalplugins_gui(gpointer data, gpointer user_data) {
	Towlgalwin *owlgalwin = user_data;
	TOwlGalunggungPlugin *owlgalplugin = data;
	if (owlgalplugin->init_gui) {
		DEBUG_MSG("owlgalplugins_gui, init_gui for plugin %p and owlgalwin %p\n",owlgalplugin,owlgalwin);	
		owlgalplugin->init_gui(owlgalwin);
	}
}
/* can be called by g_list_foreach() */
void owlgalplugins_enforce_session(gpointer data, gpointer user_data) {
	Towlgalwin *owlgalwin = user_data;
	TOwlGalunggungPlugin *owlgalplugin = data;
	if (owlgalplugin->enforce_session) {
		DEBUG_MSG("owlgalplugins_enforce_session, enforce_session for plugin %p and owlgalwin %p\n",owlgalplugin,owlgalwin);
		owlgalplugin->enforce_session(owlgalwin);
	}
}

GHashTable *owlgalplugins_register_globses_config(GHashTable *list) {
	GSList *tmplist = main_v->plugins;
	while (tmplist) {
		TOwlGalunggungPlugin *owlgalplugin = tmplist->data;
		if (owlgalplugin->register_globses_config) {
			list = owlgalplugin->register_globses_config(list);
		}
		tmplist =  g_slist_next(tmplist);
	}
	return list;
}
GHashTable *owlgalplugins_register_session_config(GHashTable *list, Tsessionvars *session) {
	GSList *tmplist = main_v->plugins;
	while (tmplist) {
		TOwlGalunggungPlugin *owlgalplugin = tmplist->data;
		if (owlgalplugin->register_session_config) {
			list = owlgalplugin->register_session_config(list, session);
		}
		tmplist =  g_slist_next(tmplist);
	}
	return list;
}

void owlgalplugins_session_cleanup(Tsessionvars *session) {
	GSList *tmplist = main_v->plugins;
	while (tmplist) {
		TOwlGalunggungPlugin *owlgalplugin = tmplist->data;
		if (owlgalplugin->session_cleanup) {
			/*g_print("cleanup session %p for plugin %p\n",session,owlgalplugin);*/
			owlgalplugin->session_cleanup(session);
		}
		tmplist =  g_slist_next(tmplist);
	}
}

void owlgalunggung_cleanup_plugins(void) {
	GSList *tmplist = main_v->plugins;
	while (tmplist) {
		TOwlGalunggungPlugin *owlgalplugin = tmplist->data;
		if (owlgalplugin->cleanup) {
			owlgalplugin->cleanup();
		}
		g_free(PRIVATE(owlgalplugin)->filename);
		g_module_close(PRIVATE(owlgalplugin)->module);
		tmplist =  g_slist_next(tmplist);
	}
	g_slist_free(main_v->plugins);
	
}
